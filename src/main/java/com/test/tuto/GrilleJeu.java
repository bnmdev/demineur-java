package com.test.tuto;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class GrilleJeu {

    public static final int CASE_VIDE = 1;
    public static final int CASE_BOMBE = 2;
    public static final int CASE_BOMBE_DECOUVERTE = 3;
    public static final int CASE_DECOUVERTE = 10;

    public int nombreBombes = 0;

    private static final int TAILLE_HORIZONTALE = 5;
    private static final int TAILLE_VERTICALE = 5;

    public List<List> initialiserGrille() {

        ArrayList<List> grille = new ArrayList<>();

        for(int i = 0; i < TAILLE_VERTICALE; i++) {
            grille.add(this.initialiserLigne());
        }

        System.out.println("Nombre de bombes : " + nombreBombes);

        return grille;
    }

    private List initialiserLigne() {
        ArrayList<Integer> cases = new ArrayList<>();

        for(int i = 0; i < TAILLE_HORIZONTALE; i++) {
            int valeurCase = this.initialiserCase();
            cases.add(valeurCase);

            if (valeurCase == CASE_BOMBE) {
                nombreBombes++;
            }
        }
        return cases;
    }

    private int initialiserCase() {

        int max = 30;
        int min = 0;

        int random = ThreadLocalRandom.current().nextInt(min, max + 1);

        // 1 chance sur 3 d'avoir une bombe
        if (random <= 10) {
            return CASE_BOMBE;
        } else {
            return CASE_VIDE;
        }
    }
}
