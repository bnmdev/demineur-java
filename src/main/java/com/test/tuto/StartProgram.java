package com.test.tuto;

import java.io.IOException;
import java.util.List;

public class StartProgram {

    public static void main(String [] args) throws IOException {

        Affichage affichage = new Affichage();
        GrilleJeu grilleJeu = new GrilleJeu();
        GestionPartie gestionPartie = new GestionPartie();

        System.out.println("####################################################");
        System.out.println("##                DEMINEUR                        ##");
        System.out.println("####################################################");

        List<List> grille = grilleJeu.initialiserGrille();

        Integer[] valeursSaisies = null;

        while(affichage.afficherGrille(valeursSaisies, grille)) {
            valeursSaisies = gestionPartie.demanderReferenceCase();
        }
    }
}
