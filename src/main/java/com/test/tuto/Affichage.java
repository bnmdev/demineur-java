package com.test.tuto;

import java.util.List;

public class Affichage {

    public boolean afficherGrille(Integer[] referenceCase, List<List> grille) {

        boolean bombeDecouverte = false;

        if (referenceCase != null) {
            int columnNumber = referenceCase[0];
            int rowNumber = referenceCase[1];

            if (grille.get(rowNumber).get(columnNumber).equals(GrilleJeu.CASE_BOMBE)) {
                grille.get(rowNumber).set(columnNumber, GrilleJeu.CASE_BOMBE_DECOUVERTE);
                bombeDecouverte = true;
            } else {
                grille.get(rowNumber).set(columnNumber, GrilleJeu.CASE_DECOUVERTE);
            }
        }

        int compteurLigne = 0;

        for (List ligne : grille) {
            List<Integer> ligneInt = (List<Integer>) ligne;

            // afficher le numéro des cases uniquement sur la 1ere ligne
            if (compteurLigne == 0) {
                int compteurCase = 0;

                System.out.print(" ");

                // debut ligne
                for (Integer caseGrille : ligneInt) {

                    System.out.print(" " + compteurCase + " ");
                    compteurCase++;
                }
                System.out.println();
            }

            System.out.print(compteurLigne);

            // debut ligne
            int compteurCase = 0;
            for (Integer caseGrille : ligneInt) {

                if (caseGrille.equals(GrilleJeu.CASE_DECOUVERTE)) {
                    System.out.print("   ");
                } else if(caseGrille.equals(GrilleJeu.CASE_BOMBE_DECOUVERTE)) {
                    System.out.print(" X ");
                } else {
                    System.out.print("[ ]");
                }

                compteurCase++;
            }
            System.out.println();
            compteurLigne++;
        }

        if (bombeDecouverte) {

            System.out.println("--------------------------");
            System.out.println("Vous êtes tombé sur une bombe. Fin de la partie.");
            System.out.println("--------------------------");

            return false;
        } else {
            return true;
        }
    }
}
