package com.test.tuto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class GestionPartie {

    public Integer[] demanderReferenceCase() throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

     //   try (Scanner scanner = new Scanner(System.in)) {

            System.out.println("Saisir numéro de case (ex. 0-2) : ");
            String reference = br.readLine(); //scanner.nextLine();

            if (reference == null || reference.length() != 3 || reference.split("-").length != 2) {
                System.out.println("La valeur saisie est incorrecte. Voici quelques exemples de valeurs correcte : 0-1, 5-6, 8-0");
                demanderReferenceCase();
            } else {
                String[] valeurs = reference.split("-");

                int columnNumber = Integer.parseInt(valeurs[0]);
                int rowNumber = Integer.parseInt(valeurs[1]);

                return new Integer[]{columnNumber, rowNumber};
            }
     //   }
        return null;
    }
}
